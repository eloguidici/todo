const jwt = require('jsonwebtoken');
var config = require('./../config');
var debug = require('debug')('Debug index => ');



function authenticate(req, res, next) {
  debug('authenticate');
  var token = req.headers['x-access-token'];
  if (!token) return res.status(401).send({
    auth: false,
    message: 'No token provided.'
  });

  jwt.verify(token, config.secret, function (err, decoded) {
    if (err) return res.status(500).send({
      auth: false,
      message: 'Failed to authenticate token.'
    });

    next();
  });
}






module.exports = authenticate;