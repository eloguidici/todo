
var logger = function (req, res, next) {

    console.info(req.method + "--" + req.originalUrl) ;
    
    res.on('finish', () => {
        console.info( res.statusCode + "--" + res.statusMessage);
    })
    
  next();
};

module.exports = logger;