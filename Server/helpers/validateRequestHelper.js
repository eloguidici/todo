'use strict';  

  const {
    celebrate,
    Joi,
    errors
} = require('celebrate');


const validateSchema = (schema) => (req, res, next) => {
    Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            // send a 422 error response if validation fails
            res.status(422).json({
                status: 'error',
                message: 'Invalid request data',
                data: err.message
            });
        } else {
            next();
        }
    });
};
	
module.exports = validateSchema  ;