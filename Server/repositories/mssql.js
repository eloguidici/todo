const sql = require("msnodesqlv8");
const format = require('string-format');
format.extend(String.prototype, {});

const connectionString = "server=localhost\\SQLEXPRESS;Database=TestTODO;Trusted_Connection=Yes;Driver={SQL Server Native Client 11.0}";


var factoryQueryPromise = (connectionString, query) => {
    return new Promise((resolve, reject) => {
        sql.query(connectionString, query, (error, rows) => {
            if (error) {
                reject(error);
            } else { ;
                resolve(rows);
            }
        });
    })
};

function getById(id) {
    const query = "SELECT * FROM Todo WHERE Id = {0}".format(id);
    return factoryQueryPromise(connectionString, query);
};

function getAll() {
    const query = "SELECT * FROM Todo";
    return factoryQueryPromise(connectionString, query);
};


function add(todo) {
    const query = "INSERT INTO [Todo]([Description],[Status],[File]) VALUES ('{0}','{1}','{2}')"
        .format(todo.description, todo.status, todo.file);
    console.log(query);
    return factoryQueryPromise(connectionString, query);
};

function update(todo) {
    const query = "UPDATE [Todo] SET [Description] = '{0}' ,[Status] = '{1}' ,[File] = '{2}' WHERE [Id] = {3}"
        .format(todo.description, todo.status, todo.file, todo.id);
    console.log(query);
    return factoryQueryPromise(connectionString, query);
};

function updateStatus(todo) {
    const query = "UPDATE [Todo] SET [Status] = '{0}' WHERE [Id] = {1}"
        .format(todo.status,todo.id);
    console.log(query);
    return factoryQueryPromise(connectionString, query);
};

function remove(id) {
    const query = "DELETE FROM Todo WHERE [Id] = {0}".format(id);
    console.log(query);
    return factoryQueryPromise(connectionString, query);
};

  module.exports = { getById, getAll, add, update, remove , updateStatus } ;