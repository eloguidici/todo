const Sequelize = require('sequelize');

var Database = new Sequelize('test-todo', 'root', null, {
	 host: '127.0.0.1',
  dialect: 'mysql',
  define: {
        timestamps: false
    }
});
  
  module.exports = {
	  Database, DataTypes: Sequelize.DataTypes
	  
  } ;