'use strict';
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const Todo = sequelize.define('todo', {
            id: {
                type: DataTypes.SMALLINT,
                autoIncrement: true,
                primaryKey: true
            },
            status: DataTypes.SMALLINT,
            file: DataTypes.STRING,
            description: DataTypes.STRING
        }, {
            tableName: 'todo'
        });
    Todo.associate = function (models) {
        // associations can be defined here
    };
    return Todo;
};
