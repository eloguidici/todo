const app = require("express")();
var bodyParser = require('body-parser');
const dotenv = require('dotenv');

const jwt = require('jsonwebtoken');
var config = require('./config');
var debug = require('debug')('Debug index => ');

var authController = require('./controllers/authController');
var todoController = require('./controllers/todoWithORMController')
var todoWithSqlController = require('./controllers/todoWithSqlController')

var authenticateHelper = require('./helpers/authenticateHelper');
var logHelper = require('./helpers/logHelper');

dotenv.config();
const PORT = process.env.PORT;

app.use(logHelper);

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies


app.get(['/json*', '/favicon.ico'], function(req, res) { res.status(204); });

app.use('/api/todo',authenticateHelper, todoController);
app.use('/api/todoVersionTwo',authenticateHelper, todoWithSqlController);

app.use('/api/auth', authController);

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).json(JSON.stringify(err.stack));
});


app.listen(PORT, () => {
  console.log("=> Server running on port:" + PORT);
});