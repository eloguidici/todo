var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');

router.post('/sign', function (req, res) {

    const {
        username,
        password
    } = req.body;

    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
	
    var token = jwt.sign({
            username,
            hashedPassword
        }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });

    res.status(200).send({
        auth: true,
        token: token
    });
});

router.get('/me', function (req, res) {
    var token = req.headers['x-access-token'];
    if (!token)
        return res.status(401).send({
            auth: false,
            message: 'No token provided.'
        });

    jwt.verify(token, config.secret, function (err, decoded) {
        if (err)
            return res.status(500).send({
                auth: false,
                message: 'Failed to authenticate token.'
            });

        res.status(200).send(decoded);
    });
});

module.exports = router;
