'use strict';

const {
    celebrate,
    Joi,
    errors
} = require('celebrate');

const idError = 'Id is required';
const descriptionError = 'Description is required';
const fileError = 'File is required';
const statusError = 'Status is required';

const todoAddSchema = Joi.object().keys({
        description: Joi.string().required().error(new Error(descriptionError)),
        status: Joi.number().integer().default(1),
        file: Joi.string().required().error(new Error(fileError))
    });

const todoUpdateSchema = Joi.object().keys({
        id: Joi.number().integer().required().error(new Error(idError)),
        description: Joi.string().required().error(new Error(descriptionError)),
        status: Joi.number().integer().required().error(new Error(statusError)),
        file: Joi.string().required().error(new Error(fileError))
    });

const todoUpdateStatusSchema = Joi.object().keys({
        id: Joi.number().integer().required().error(new Error(idError)),
        status: Joi.number().integer().required().error(new Error(statusError))
    });

module.exports = {
    todoAddSchema,
    todoUpdateSchema,
    todoUpdateStatusSchema
};
