const router = require('express').Router();
const debug = require('debug')('Debug controller => ');
const multer = require('multer');
const {  celebrate, Joi, errors} = require('celebrate');
const repository = require('../repositories/mssql.js');
const validateSchema = require('../helpers/validateRequestHelper.js');
const  { todoAddSchema, todoUpdateSchema , todoUpdateStatusSchema } = require('./requestSchemas/todo.js');
var upload = multer({   dest: 'uploads/' });
var Model = {};

const fileName = 'myFile';


//POST UPLOAD FILE
router.post('/file', upload.single(fileName), (req, res, next) => {
    const file = req.file;
    if (!file) {
        res.status(400).json('Please upload a file');
    }
    res.status(200).json(file);
});

//POST
router.post('/', validateSchema(todoAddSchema), async function (req, res) {
    repository.add({
		status: req.body.status|1,
        file: req.body.file,
        description: req.body.description
    })
    .then(() => {
        res.status(200).send();
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

//PUT
router.put('/:id', validateSchema(todoUpdateSchema), async function (req, res) {
    repository.update({
		id:req.body.id,
		status: req.body.status,
        file: req.body.file,
        description: req.body.description
    }).then(() => {
           res.status(200).send();
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

//GET ID
router.get('/:id', async function (req, res) {	
	repository.getById(req.params.id)
    .then(todo => {
		debug(todo);
        res.status(200).json(todo);
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    })
});

//GET
router.get('/', async function (req, res) {
	repository.getAll()
    .then(todo => {
		debug(todo);
        res.status(200).json(todo);
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    })
});

//DELETE
router.delete('/:id', async function (req, res) {
    repository.remove(req.params.id)
	.then(() => {
         res.status(200).send();
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

//PATCH
router.patch('/status/:id', validateSchema(todoUpdateStatusSchema), async function (req, res) {
    repository.updateStatus({
		id:req.body.id,
        status: req.body.status,
    }, {
        where: {
            id: req.params.id
        }
    }).then(() => {
       res.status(200).send();
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

module.exports = router;
