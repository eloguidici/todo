const router = require('express').Router();
const debug = require('debug')('Debug controller => ');
const multer = require('multer');
const {  celebrate, Joi, errors} = require('celebrate');
const sequelize = require('../repositories/mysql.js');
const validateSchema = require('../helpers/validateRequestHelper.js');
const  { todoAddSchema, todoUpdateSchema , todoUpdateStatusSchema } = require('./requestSchemas/todo.js');
var upload = multer({   dest: 'uploads/' });
var Model = {};

const fileName = 'myFile';

sequelize.Database.authenticate()
.then(() => {
    Model = require('../repositories/todoModel.js')(sequelize.Database, sequelize.DataTypes);
    console.log('Conectado');
})
.catch((err) => {
    console.log('No se conecto');
	console.log(err);
	process.exit(1);
});

//POST UPLOAD FILE
router.post('/file', upload.single(fileName), (req, res, next) => {
    const file = req.file;
    if (!file) {
        res.status(400).json('Please upload a file');
    }
    res.status(200).json(file);
});

//POST
router.post('/', validateSchema(todoAddSchema), async function (req, res) {
    Model.create({
        file: req.body.file,
        description: req.body.description,
        status: req.body.status | 1
    })
    .then((todo) => {
        debug(todo);
        res.status(200).json(todo);
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

//PUT
router.put('/:id', validateSchema(todoUpdateSchema), async function (req, res) {
    Model.update({
        file: req.body.file,
        description: req.body.description
    }, {
        where: {
            id: req.params.id
        }
    }).then((todo) => {
        debug(todo);
        res.status(200).json(todo);
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

//GET ID
router.get('/:id', async function (req, res) {
    Model.findOne({
        attributes: ['id', 'file', 'description', 'status'],
        where: {
            'id': req.params.id
        }
    })
    .then(todo => {
		debug(todo);
        res.status(200).json(todo);
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    })
});

//GET
router.get('/', async function (req, res) {
    Model.findAll({
        attributes: ['id', 'file', 'description', 'status']
    })
    .then(todo => {
		debug(todo);
        res.status(200).json(todo);
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    })
});

//DELETE
router.delete('/:id', async function (req, res) {
    Model.destroy({
        where: {
            id: req.params.id
        }
    }).then((todo) => {
        	debug(todo);
        res.status(200).json('');
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

//PATCH
router.patch('/status/:id', validateSchema(todoUpdateStatusSchema), async function (req, res) {
    Model.update({
        status: req.body.status,
    }, {
        where: {
            id: req.params.id
        }
    }).then((todo) => {
        	debug(todo);
        res.status(200).json(todo);
    })
    .catch(err => {
        debug(err);
        res.status(500).json(err);
    });
});

module.exports = router;
